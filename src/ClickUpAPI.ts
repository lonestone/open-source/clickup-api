import Axios, { AxiosInstance } from "axios";
import {
  ClickupList,
  ClickupListCreation,
  ClickupProject,
  ClickupSpace,
  ClickupTask,
  ClickupTaskCreation,
  ClickupTeam,
  ClickupUser,
  ClickupComment
} from "./interfaces/ClickUpInterfaces";
export * from "./interfaces/ClickUpInterfaces";

/**
 * As the API is still a WIP this will use some hacks there and there
 * The biggest one is the use of both the user public api key (official api) and a bearer token (non-official api)
 */
export class ClickUp {
  private bearer: string;
  private publicKey: string;
  private domain: string;
  private officialApiDomain: string = "https://api.clickup.com/api/v1/";

  private hackAxios: AxiosInstance;
  private officialAxios: AxiosInstance;

  constructor(
    publicKey: string,
    bearerToken: string,
    domain: string,
    officialApiDomain?: string
  ) {
    this.bearer = bearerToken;
    this.publicKey = publicKey;
    this.domain = domain;
    if (officialApiDomain) this.officialApiDomain = officialApiDomain;

    this.hackAxios = Axios.create({
      baseURL: this.domain,
      timeout: 30000,
      headers: { Authorization: "Bearer " + this.bearer }
    });

    this.officialAxios = Axios.create({
      baseURL: this.officialApiDomain,
      timeout: 30000,
      headers: { Authorization: this.publicKey }
    });
  }

  public async getTeams(): Promise<ClickupTeam[]> {
    return (await this.officialAxios.get("/team")).data.teams;
  }

  public async getTeamUsers(teamId: string): Promise<ClickupUser[]> {
    return (await this.hackAxios.get("/team/" + teamId)).data.members.map(
      (m: any) => m.user
    );
  }

  public async getTeamSpaces(teamId: string): Promise<ClickupSpace[]> {
    return (await this.officialAxios.get("/team/" + teamId + "/space")).data
      .spaces;
  }

  public async getSpaceProjects(spaceId: string): Promise<ClickupProject[]> {
    return (await this.officialAxios.get("/space/" + spaceId + "/project")).data
      .projects;
  }

  public async getListTasks(listId: string, teamId: string) {
    return (await this.officialAxios.get("/team/" + teamId + "/task", {
      params: {
        list_ids: [listId],
        subtasks: true
      }
    })).data.tasks;
  }

  public async createTask(
    newTask: ClickupTaskCreation,
    listId: string
  ): Promise<ClickupTask> {
    return (await this.hackAxios.post<ClickupTask>(
      "/subcategory/" + listId + "/task",
      newTask
    )).data;
  }

  public async commentTask(
    taskId: string,
    comment: ClickupComment
  ): Promise<{ id: string }> {
    return (await this.hackAxios.post<{ id: string }>(
      "/task/" + taskId + "/comment",
      comment
    )).data;
  }

  public async createList(
    newList: ClickupListCreation,
    projectId: string
  ): Promise<ClickupList> {
    return (await this.officialAxios.post<ClickupList>(
      "/project/" + projectId + "/list",
      newList
    )).data;
  }
}
