export enum Status {
  Open = "Open",
  InProgress = "in progress",
  InReview = "in review",
  Blocked = "blocked",
  Closed = "Closed"
}

export enum Priorities {
  Urgent = "1",
  High = "2",
  Normal = "3",
  Low = "4"
}

export interface ClickupPriority {
  id: Priorities;
  priority: string;
  color: string;
  orderindex: string;
}

export interface ClickupUser {
  id: number;
  username: string;
  color: string;
  profilePicture: string;
  initials: string;
  email: string;
}

export interface ClickupTag {
  name: string;
  tag_fg: string;
  tag_bg: string;
}

export interface ClickupStatus {
  status: Status;
  color: string;
  type: "open" | "blocked" | "custom";
  orderindex: number;
}

export interface ClickupTaskCreation {
  name: string;
  content?: string;
  assignees?: string[]; // user ids
  status?: Status;
  priority?: Priorities;
  due_date?: number;
  tags?: ClickupTag[];
  subtasks?: ClickupTaskCreation[];
}

export interface ClickupTask {
  id: string;
  name: string;
  text_content: string | null;
  status: ClickupStatus;
  orderindex: string;
  date_created: number;
  date_updated: number;
  date_closed: number | null;
  creator: ClickupUser;
  assignees: ClickupUser[];
  tags: ClickupTag[];
  parent: string | null; // id
  priority: ClickupPriority | null;
  due_date: number | null;
  start_date: number | null;
  time_estimate: number | null;
  list: { id: string };
  project: { id: string };
  space: { id: string };
  url: string;
}

export interface ClickupTeam {
  id: string;
  name: string;
  color: string;
  avatar: string;
  members: Array<{
    user: ClickupUser;
  }>;
}

export interface ClickupSpace {
  id: string;
  name: string;
  private: boolean;
  statuses: ClickupStatus[];
  multiple_assignees: boolean;
}

export interface ClickupProject {
  id: string;
  name: string;
  override_statuses: boolean;
  statuses: ClickupStatus[];
  lists: ClickupList[];
}

export interface ClickupListCreation {
  name: string;
}

export interface ClickupList {
  id: string;
  name: string;
}

export interface ClickupComment {
  attachment: [];
  comment: [
    {
      text: string;
    }
  ];
}
